module.exports = {
  development: {
    username: process.env.POSTGRES_USER || 'admin_core',
    password: process.env.POSTGRES_PASS || 'admin_core',
    database: process.env.POSTGRES_DB || 'api_core',
    host: process.env.POSTGRES_HOST || '127.0.0.1',
    port : process.env.POSTGRES_PORT || '5432',
    dialect: 'postgres',
    logging : null
  },
  test: {
    username: process.env.POSTGRES_USER || 'admin_core',
    password: process.env.POSTGRES_PASS || 'admin_core',
    database: process.env.POSTGRES_DB || 'api_core',
    host: process.env.POSTGRES_HOST || '127.0.0.1',
    dialect: 'postgres',
    logging: null
  },
  production: {
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASS,
    database: process.env.POSTGRES_DB,
    host: process.env.POSTGRES_HOST,
    port : process.env.POSTGRES_PORT,
    dialect: 'postgres',
    logging : false 
  },
};
