require('dotenv').config();

const ENV = process.env.NODE_ENV || 'development';
const server = Object.assign({
  port: process.env.NODE_PORT || '3000',
  version: process.env.API_VERSION || '1.0.0',
});
const pgConfig = require('./database')[ENV];
const mongoConfig = Object.assign({
  username: process.env.MONGO_USER || 'admin_auth',
  password: process.env.MONGO_PASS || 'admin_auth',
  database: process.env.MONGO_DB || 'auth',
  host: process.env.MONGO_HOST || '127.0.0.1',
  port: process.env.MONGO_PORT || '27017',
});

const config = Object.assign({
  [ENV]: true,
  env: ENV,
  server,
  pgConfig,
  mongoConfig,
});

module.exports = config;
