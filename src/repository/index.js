module.exports = {
  // Sequelize
  SequelizeApplicationsRepository: require('./SequelizeApplicationsRepository'),
  SequelizeUsersRepository: require('./SequelizeUsersRepository'),
  SequelizeTaberRepository: require('./SequelizeTaberRepository'),
  // Mongoose
  MongooseUsersRepository: require('./MongooseUsersRepository'),
  MongooseTokenRepository: require('./MongooseTokenRepository'),
};
