const uuid = require('uuid/v4');

class SequelizeTaberRepository {
  constructor({
    logger,
    constants,
    jointSavingMemberModel,
    jointSavingAccountModel,
    jointSavingAccountGroupModel,
    jointSavingAccountTransactionModel,
    jointSavingRatingModel,
  }) {
    this.logger = logger;
    this.jointSavingMemberModel = jointSavingMemberModel;
    this.jointSavingAccountModel = jointSavingAccountModel;
    this.jointSavingAccountGroupModel = jointSavingAccountGroupModel;
    this.jointSavingAccountTransactionModel = jointSavingAccountTransactionModel;
    this.jointSavingRatingModel = jointSavingRatingModel;

    // Declare specific constants
    this.JS_MEMER_TYPE = constants.JOINT_SAVING_MEMBER_TYPE;
  }

  async createJSA(data, groups) {
    this.logger.info('Taber Repository - Create JointSavingAccount', data);
    const transaction = await this.jointSavingAccountModel.sequelize.transaction();
    try {
      const jointSavingAccount = await this.jointSavingAccountModel.create(this._toDatabase(data));
      this.logger.debug('JointSavingAccount', jointSavingAccount);
      // assign the creator id as ADM_COLLECTOR in jointSavingMember
      await this.jointSavingMemberModel.create({
        user_id: data.creator_id,
        joint_saving_account_id: jointSavingAccount.id,
        type: JS_MEMER_TYPE.ADM_COLLECTOR,
      });
      // Create jointSavingAccountGroup
      for (const group of groups) {
        await this.jointSavingAccountGroupModel.create({
          id: uuid(),
          joint_saving_account_id: jointSavingAccount.id,
          name: group.name,
          balance: 0, // Initiate zero
        });
      }
      transaction.commit();
      return this._toEntity(jointSavingAccount);
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  async addMember(user_id, joint_saving_account_id) {
    this.logger.info('Taber Repository - add member', { user_id, joint_saving_account_id, type });
    const transaction = await this.jointSavingMemberModel.sequelize.transaction();
    try {
      const jointSavingMember = await this.jointSavingMemberModel.create({
        user_id,
        joint_saving_account_id,
        type: this.JS_MEMER_TYPE.SAVER,
      });
      await transaction.commit();
      return jointSavingMember;
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  async assignCollector(user_id, joint_saving_account_id) {
    this.logger.info('Taber Repository - assign collector', { user_id, joint_saving_account_id });
    const transaction = await this.jointSavingMemberModel.sequelize.transaction();
    try {
      const update = await this.jointSavingMemberModel.update({
        type: this.JS_MEMER_TYPE.COLLECTOR,
      }, { where: { user_id, joint_saving_account_id } });
      await transaction.commit();
      return update;
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  // Others
  _toEntity(joint_saving_account) {
    const {
      id,
      creator_id,
      type,
      status,
      balance,
      name,
      currency,
      description,
      target_savers,
      target_amount_savers,
      saving_target_time,
      saving_deposit_time,
      saving_deposit_type,
    } = joint_saving_account;

    return {
      id,
      creatorId: creator_id,
      type,
      status,
      balance,
      name,
      currency,
      description,
      targetSavers: target_savers,
      targetAmountSavers: target_amount_savers,
      savingTargetTime: saving_target_time,
      savingDepositTime: saving_deposit_time,
      savingDepositType: saving_deposit_type,
    };
  }

  _toDatabase(jointSavingAccount) {
    const {
      id,
      creatorId,
      type,
      status,
      balance,
      name,
      currency,
      description,
      targetSavers,
      targetAmountSavers,
      savingTargetTime,
      savingDepositTime,
      savingDepositType,
    } = jointSavingAccount;

    return {
      id,
      creator_id: creatorId,
      type,
      status,
      balance,
      name,
      currency,
      description,
      target_savers: targetSavers,
      target_amount_savers: targetAmountSavers,
      saving_target_time: savingTargetTime,
      saving_deposit_time: savingDepositTime,
      saving_deposit_type: savingDepositType,
    };
  }
}

module.exports = SequelizeTaberRepository;