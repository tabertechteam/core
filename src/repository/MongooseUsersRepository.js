class MongooseUsersRepository {
  constructor({ logger, mongoose, userSchema }) {
    this.logger = logger;
    // this.mongoose = mongoose;
    this.userSchema = userSchema;
  }

  async insert(data) {
    this.logger.info('Mongoose Users Repository - Insert', data);
    const session = await this.userSchema.startSession();
    await session.startTransaction();
    try {
      const user = new this.userSchema(data);
      await session.commitTransaction();
      return user.save();
    } catch (error) {
      await session.abortTransaction();
      throw error;
    }
  }

  async findAll(options = {}) {
    this.logger.info('Mongoose Users Repository - find all', options);
    try {
      const users = await this.userSchema.find(options);
      return users;
    } catch (error) {
      throw error;
    }
  }

  async findOne(options = {}) {
    this.logger.info('Mongoose Users Repository - find one', options);
    try {
      const user = await this.userSchema.findOne(options);
      return user;
    } catch (error) {
      throw error;
    }
  }

}

module.exports = MongooseUsersRepository;
