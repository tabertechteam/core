class SequelizeApplicationsRepository {
  constructor({ logger, applicationModel }) {
    this.logger = logger;
    this.applicationModel = applicationModel;
  }

  async create(id, user_id, status, remarks) {
    this.logger.info('Application Repository - Create', { id, user_id, status, remarks });
    const transaction = await this.applicationModel.sequelize.transaction();
    try {
      const application = await this.applicationModel.create({
        id,
        user_id,
        status,
        remarks,
      });
      await transaction.commit();
      return this._toEntity(application);
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  async update(application) {
    this.logger.info('Application Repository - Update', { application });
    const transaction = await this.applicationModel.sequelize.transaction();
    try {
      const update = await this.applicationModel.update(
        this._toDatabase(application),
        { where: { id: application.id }}
      );
      await transaction.commit();
      return update;
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  async getByUserId(user_id) {
    this.logger.info('Application Repository - getByUserId', { user_id });
    try {
      const application = await this.applicationModel.findOne({ where: { user_id } });
      if (application) {
        return this._toEntity(application);
      } else {
        return null;
      }
    } catch (error) {
      throw error;
    }
  }

  // Others
  _toEntity(application) {
    const { id, user_id, status, remarks } = application;

    return {
      id,
      userId: user_id,
      status,
      remarks,
    };
  }

  _toDatabase(application) {
    const { id, userId, status, remarks } = application;
    
    return {
      id,
      user_id: userId,
      status,
      remarks,
    };
  }
}

module.exports = SequelizeApplicationsRepository;
