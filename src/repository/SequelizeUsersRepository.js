class SequelizeUsersRepository {
  constructor({ logger, userModel }) {
    this.logger = logger;
    this.userModel = userModel;
  }

  async create(id, full_name, phone_number, email = null, gender = null, isRegister) {
    this.logger.info('Application Repository - Create', { id, full_name, phone_number, email, gender });
    const transaction = await this.userModel.sequelize.transaction();
    try {
      const user = await this.userModel.create({
        id,
        full_name,
        phone_number,
        email,
        gender,
        isRegister,
      });
      await transaction.commit();
      return this._toEntity(user);
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  async changePin(id, pin) {
    this.logger.info('Application Repository - changePin', { id, pin });
    const transaction = await this.userModel.sequelize.transaction();
    try {
      const update = await this.userModel.update({
        pin,
      }, { where: { id} });
      await transaction.commit();
      return update;
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  async changePassword(id, password) {
    this.logger.info('Application Repository - changePassword', { id, password });
    const transaction = await this.userModel.sequelize.transaction();
    try {
      const update = await this.userModel.update({
        password,
      }, { where: { id } });
      await transaction.commit();
      return update;
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  async getByPhoneNumber(phone_number) {
    this.logger.info('Application Repository - getByPhoneNumber', { phone_number });
    try {
      const user = await this.userModel.findOne({ where: { phone_number } });
      if (user) {
        return this._toEntity(user);
      } else {
        return null;
      }
    } catch (error) {
      throw error;
    }
  }

  // Others
  _toEntity(user) {
    const { id, pin, password, full_name, phone_number, email, gender } = user;

    return {
      id,
      pin,
      password,
      fullName: full_name,
      phoneNumber: phone_number,
      email,
      gender,
    };
  }
}

module.exports = SequelizeUsersRepository;
