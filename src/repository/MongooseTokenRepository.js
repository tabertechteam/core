class MongooseTokenRepository {
  constructor({ logger, mongoDB, mongoSchema }) {
    this.logger = logger;
    this.mongoDB = mongoDB;
    this.tokenSchema = mongoSchema.token;
  }

  async insert(data) {
    this.logger.info('Mongoose Users Repository - Insert', data);
    const session = await this.tokenSchema.startSession();
    await session.startTransaction();
    try {
      const token = await new this.tokenSchema(data);
      await session.commitTransaction();
      return token.save();
    } catch (error) {
      await session.abortTransaction();
      throw error;
    }
  }

  async findAll(options = {}) {
    this.logger.info('Mongoose Users Repository - find all', options);
    try {
      const token = await this.tokenSchema.find(options);
      return token;
    } catch (error) {
      throw error;
    }
  }

  async findOne(options = {}) {
    this.logger.info('Mongoose Users Repository - find one', options);
    try {
      const token = await this.tokenSchema.findOne(options);
      return token;
    } catch (error) {
      throw error;
    }
  }
  
}

module.exports = MongooseTokenRepository;
