const express = require('express');

class Server {
  constructor({ config, database, router, logger }) {
    this.config = config;
    this.database = database;
    this.logger = logger;
    this.express = express();
    this.express.disable('x-powered-by');
    this.express.use(router);
  }

  async start() {
    if(this.database) {
      await this.database.authenticate();
    }
    
    return new Promise((resolve) => {
      const http = this.express
        .listen(this.config.server.port, () => {
          const { port } = http.address();
          this.logger.info(`Listening at port ${port}`);
          resolve();
        });
    });
  }

}

module.exports = Server;
