const { Router } = require('express');
const { inject } = require('awilix-express');
const cors = require('cors');
const bodyParser = require('body-parser');
const routes = require('./createRoutes');

module.exports = ({
  config,
  logger, 
  containerMiddleware,
  authorizationMiddlewares,
}) => {
  const router = Router();
  const v1Router = Router();

  router
    .use(cors())
    .use(bodyParser.json())
    .use(containerMiddleware)
    
  router.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
      next();
    });

  router
    .get('/', (req, res) => {
      res.json({ name: 'Taber Core API', version: config.server.version });
    });

  // Routes v1
  v1Router
    .get('/', (req, res) => {
      res.json({ name: 'Taber Core API Version 1'});
    });
  v1Router.use('/public', routes('v1/PublicRoutes'));
  v1Router.use('/private', authorizationMiddlewares, routes('v1/PrivateRoutes'));

  router.use('/v1', v1Router);

  if (process.env.NODE_ENV == 'development') {
    const listEndpoints = require('express-list-endpoints')
    console.log(listEndpoints(router));
  }

  return router;
};
