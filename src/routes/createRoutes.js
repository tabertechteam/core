const path = require('path');

module.exports = function createRoutes(routesUri) {
  const routesPath = path.resolve('src/routes', routesUri);
  const Routes = require(routesPath);
  
  return Routes.router;
};
