const { Router } = require('express');
const { inject } = require('awilix-express');
const Status = require('http-status');

const PrivateRoutes = {
  get router() {
    const router = Router();

    router.post('/taber/create', this.createTaber);

    return router;
  },

  createTaber(req, res, next) {
    const { taberControllers } = req;
    const { SUCCESS, ERROR } = taberControllers.outputs;
    const {
      taberName,
      taberCurrency,
      taberDescription,
      taberTargetSavers,
      taberTargetAmountSavers,
      taberSavingTargetTime,
      taberSavingDepositTime,
      taberSavngDepositType,
      taberSavingGroup,
      memberInvitationList,
    } = req.body;

    // Hardocded
    const userId = '34f283f2-aab3-42fd-a0be-763b5354c5af';

    taberControllers
      .on(SUCCESS, (response) => {
        res
          .Status(Status.CREATED)
          .json(response)
      })
      .on(ERROR, next);

    taberControllers.createTaber(
      userId,
      taberName,
      taberCurrency,
      taberDescription,
      taberTargetSavers,
      taberTargetAmountSavers,
      taberSavingTargetTime,
      taberSavingDepositTime,
      taberSavngDepositType,
      taberSavingGroup,
      memberInvitationList,
    );
  },
};

module.exports = PrivateRoutes;