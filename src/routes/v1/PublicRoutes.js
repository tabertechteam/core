const { Router } = require('express');
const { inject } = require('awilix-express');
const Status = require('http-status');

const PublicRoutes = {
  get router() {
    const router = Router();

    router.post('/signup', inject('publicControllers'), this.signup);
    router.post('/signin', inject('publicControllers'), this.signin);

    router.post('/otp/generate', inject('publicControllers'), this.generateOTP);
    router.post('/otp/verify', inject('publicControllers'), this.verifyOTP);

    router.post('/pin/set', inject('publicControllers'), this.setPIN);
    router.post('/pin/verify', inject('publicControllers'), this.verifyPIN);

    return router;
  },

  signup(req, res, next) {
    const { publicControllers } = req;
    const { SUCCESS, SUCCESS_EXIST, ERROR } = publicControllers.outputs;
    const { fullName, phoneNumber, email, gender } = req.body;

    publicControllers
      .on(SUCCESS, (response) => {
        res
          .status(Status.CREATED)
          .json(response)
      })
      .on(SUCCESS_EXIST, (response) => {
        res
          .status(Status.ACCEPTED)
          .json(response)
      })
      .on(ERROR, next);

    publicControllers.signup(fullName, phoneNumber, email, gender);
  },

  signin(req, res, next) {
    const { publicControllers } = req;
    const { SUCCESS, REDIRECT, ERROR } = publicControllers.outputs;
    const { phoneNumber } = req.body;

    publicControllers
      .on(SUCCESS, (response) => {
        res
          .status(Status.OK)
          .json(response);
      })
      .on(REDIRECT, (response) => {
        res
          .status(Status.TEMPORARY_REDIRECT)
          .json(response);
      })
      .on(ERROR, next);

    publicControllers.signin(phoneNumber);
  },
  
  generateOTP(req, res, next) {
    const { publicControllers } = req;
    const { SUCCESS, OTP_SUSPEND, NOT_FOUND, ERROR } = publicControllers.outputs;
    const { phoneNumber, isRegister } = req.body;

    publicControllers
      .on(SUCCESS, (response) => {
        res
          .status(Status.OK)
          .json(response);
      })
      .on(OTP_SUSPEND, (response) => {
        res
          .status(Status.LOCKED)
          .json(response);
      })
      .on(NOT_FOUND, (response) => {
        res
          .status(Status.NOT_FOUND)
          .json(response);
      })
      .on(ERROR, next);

    publicControllers.generateOTP(phoneNumber, isRegister);
  },

  verifyOTP(req, res, next) {
    const { publicControllers } = req;
    const { SUCCESS, OTP_EXPIRED, OTP_NOT_VALID, ERROR } = publicControllers.outputs;
    const { phoneNumber, otp, isRegister } = req.body;

    publicControllers
      .on(SUCCESS, (response) => {
        res
          .status(Status.OK)
          .json(response);
      })
      .on(OTP_EXPIRED, (response) => {
        res
          .status(Status.LOCKED)
          .json(response);
      })
      .on(OTP_NOT_VALID, (response) => {
        res
          .status(Status.LOCKED)
          .json(response);
      })
      .on(ERROR, next);

    publicControllers.verifyOTP(phoneNumber, otp, isRegister);
  },

  setPIN(req, res, next) {
    const { publicControllers } = req;
    const { SUCCESS, NOT_FOUND, ERROR } = publicControllers.outputs;
    const { phoneNumber, pin } = req.body;

    publicControllers
      .on(SUCCESS, (response) => {
        res
          .status(Status.OK)
          .json(response);
      })
      .on(NOT_FOUND, (response) => {
        res
          .status(Status.NOT_FOUND)
          .json(response);
      })
      .on(ERROR, next);

    publicControllers.setPIN(phoneNumber, pin);
  },

  verifyPIN(req, res, next) {
    const { publicControllers } = req;
    const { SUCCESS, PIN_NOT_VALID, NOT_FOUND, ERROR } = publicControllers.outputs;
    const { phoneNumber, pin } = req.body;

    publicControllers
      .on(SUCCESS, (response) => {
        res
          .status(Status.OK)
          .json(response);
      })
      .on(PIN_NOT_VALID, (response) => {
        res
          .status(Status.FORBIDDEN)
          .json(response);
      })
      .on(NOT_FOUND, (response) => {
        res
          .status(Status.NOT_FOUND)
          .json(response);
      })
      .on(ERROR, next);

    publicControllers.verifyPIN(phoneNumber, pin);
  },

};

module.exports = PublicRoutes;
