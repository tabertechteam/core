const { Schema, model } = require('mongoose');

const UserSchema = new Schema(
  {
    user_id: {
      type: String,
      // index: true,
    },
    email: String,
    phone_number: String,
    access_token: String,
    credentials: Array,
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    strict: false
  }
);

module.exports = model('users', UserSchema);
