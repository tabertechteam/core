const { Schema, model } = require('mongoose');

const TokenSchema = Schema(
  {
    id: {
      type: String,
      // index: true,
    },
    user_id: String,
    expire_at: Date,
    created_at: Date,
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    strict: false
  }
);

module.exports = model('token', TokenSchema);
