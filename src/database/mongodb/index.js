const fs = require('fs');
const path = require('path');
const Mongoose = require('mongoose');
const basename = path.basename(__filename);
const { mongoConfig: config } = require('../../../config');

const connectionString = `mongodb://${config.username}:${config.password}@${config.host}:${config.port}/${config.database}`;
const db = {};

const mongoose = Mongoose
  .connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log(`MongoDB Connected mongodb://${config.host}:${config.port}/${config.database}`))
  .catch((error) => console.log('Error on connecting to mongodb', error || null));

fs
  .readdirSync(__dirname)
  .filter((file) => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach((file) => {
    const schema = require(path.join(__dirname, file));
    db[schema.modelName] = schema;    
  });

db.mongoose = mongoose;

module.exports = db;