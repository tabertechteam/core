'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('joint_saving_ratings', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      joint_saving_account_id: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      source_id: {
        type: Sequelize.STRING
      },
      destination_id: {
        type: Sequelize.STRING
      },
      rating: {
        type: Sequelize.INTEGER
      },
      remarks: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('joint_saving_ratings');
  }
};