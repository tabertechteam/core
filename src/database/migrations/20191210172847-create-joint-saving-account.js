'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('joint_saving_accounts', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      creator_id: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING
      },
      balance: {
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      currency: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      target_savers: {
        type: Sequelize.INTEGER
      },
      target_amount_savers: {
        type: Sequelize.INTEGER
      },
      saving_target_time: {
        type: Sequelize.DATE
      },
      saving_deposit_time: {
        type: Sequelize.STRING
      },
      saving_deposit_type: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('joint_saving_accounts');
  }
};