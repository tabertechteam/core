'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('joint_saving_account_groups', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      joint_saving_account_id: {
        type: Sequelize.STRING
      },
      joint_saving_account_group_id: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      balance: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('joint_saving_account_groups');
  }
};