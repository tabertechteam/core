'use strict';
module.exports = (sequelize, DataTypes) => {
  const joint_saving_member = sequelize.define('joint_saving_member', {
    user_id: DataTypes.STRING,
    joint_saving_account_id: DataTypes.STRING,
    type: DataTypes.STRING
  }, {
    underscored: true,
  });
  joint_saving_member.associate = function(models) {
    // associations can be defined here
  };
  return joint_saving_member;
};