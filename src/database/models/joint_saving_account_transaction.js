'use strict';
module.exports = (sequelize, DataTypes) => {
  const joint_saving_account_transaction = sequelize.define('joint_saving_account_transaction', {
    id: {
      primaryKey: true,
      type: DataTypes.STRING,
    },
    joint_saving_account_id: DataTypes.STRING,
    joint_saving_account_group_id: DataTypes.STRING,
    type: DataTypes.STRING,
    status: DataTypes.STRING,
    amount: DataTypes.INTEGER,
    source: DataTypes.STRING,
    destination: DataTypes.STRING
  }, {
    underscored: true,
  });
  joint_saving_account_transaction.associate = function(models) {
    // associations can be defined here
  };
  return joint_saving_account_transaction;
};