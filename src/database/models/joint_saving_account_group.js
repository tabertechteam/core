'use strict';
module.exports = (sequelize, DataTypes) => {
  const joint_saving_account_group = sequelize.define('joint_saving_account_group', {
    id: {
      primaryKey: true,
      type: DataTypes.STRING,
    },
    joint_saving_account_id: DataTypes.STRING,
    name: DataTypes.STRING,
    balance: DataTypes.STRING
  }, {
    underscored: true,
  });
  joint_saving_account_group.associate = function(models) {
    // associations can be defined here
  };
  return joint_saving_account_group;
};