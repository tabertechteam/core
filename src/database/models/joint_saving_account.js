'use strict';
module.exports = (sequelize, DataTypes) => {
  const joint_saving_account = sequelize.define('joint_saving_account', {
    id: {
      primaryKey: true,
      type: DataTypes.STRING,
    },
    creator_id: DataTypes.STRING,
    type: DataTypes.STRING,
    status: DataTypes.STRING,
    balance: DataTypes.INTEGER,
    name: DataTypes.STRING,
    currency: DataTypes.STRING,
    description: DataTypes.STRING,
    target_savers: DataTypes.INTEGER,
    target_amount_savers: DataTypes.INTEGER,
    saving_target_time: DataTypes.DATE,
    saving_deposit_time: DataTypes.STRING,
    saving_deposit_type: DataTypes.STRING
  }, {
    underscored: true,
  });
  joint_saving_account.associate = function(models) {
    // associations can be defined here
  };
  return joint_saving_account;
};