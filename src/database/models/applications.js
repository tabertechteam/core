'use strict';
module.exports = (sequelize, DataTypes) => {
  const applications = sequelize.define('applications', {
    id: {
      primaryKey: true,
      type: DataTypes.STRING,
    },
    user_id: DataTypes.STRING,
    status: DataTypes.INTEGER,
    remarks: DataTypes.STRING
  }, {
    underscored: true,
  });
  applications.associate = function(models) {
    applications.belongsTo(models.users, {
      targetKey: 'id',
      foreignKey: 'user_id',
    });
  };
  return applications;
};
