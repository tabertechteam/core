'use strict';
module.exports = (sequelize, DataTypes) => {
  const joint_saving_rating = sequelize.define('joint_saving_rating', {
    id: {
      primaryKey: true,
      type: DataTypes.STRING,
    },
    joint_saving_account_id: DataTypes.STRING,
    type: DataTypes.STRING,
    source_id: DataTypes.STRING,
    destination_id: DataTypes.STRING,
    rating: DataTypes.INTEGER,
    remarks: DataTypes.STRING
  }, {
    underscored: true,
  });
  joint_saving_rating.associate = function(models) {
    // associations can be defined here
  };
  return joint_saving_rating;
};