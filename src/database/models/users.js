'use strict';
module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    id: {
      primaryKey: true,
      type: DataTypes.STRING,
    },
    pin: DataTypes.STRING,
    password: DataTypes.STRING,
    full_name: DataTypes.STRING,
    phone_number: DataTypes.STRING,
    email: DataTypes.STRING,
    gender: DataTypes.STRING,
    isRegister: DataTypes.BOOLEAN
  }, {
    underscored: true,
  });
  users.associate = function(models) {
    users.hasMany(models.applications, {
      sourceKey: 'id',
      foreignKey: 'user_id',
    });
  };
  return users;
};
