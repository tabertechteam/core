const Operation = require('./Operation');
const uuid = require('uuid/v4');
const bcrypt = require('bcrypt');
const Status = require('http-status');
// Constants
const OTP_EXPIRY = 120; //Seconds
const OTP_COUNTER = 3;
const BLOCK_EXPIRY = 120; // Seconds
const APPLICATION_EXPIRY = 3600; // Seconds / 1 Hour

class PublicControllers extends Operation {
  constructor({ redisClient, logger, constants, usersRepository, applicationsRepository }) {
    super();
    this.redisClient = redisClient;
    this.logger = logger;
    this.usersRepository = usersRepository;
    this.applicationsRepository = applicationsRepository;
    
    // Declare specific constants
    this.APPLICATION_STATUS = constants.APPLICATION_STATUS;
    this.WEB_URL = constants.WEB_URL;
  }

  async signup(fullName, phoneNumber, email, gender) {
    this.logger.info('Public Controller - Sign Up', { fullName, phoneNumber, email, gender });
    const { SUCCESS, SUCCESS_EXIST, ERROR } = this.outputs;
    try {
      const checkUser = await this.usersRepository.getByPhoneNumber(phoneNumber);
      if (!checkUser) {
        // Create user
        const user = await this.usersRepository.create(
          uuid(),
          fullName,
          phoneNumber,
          email,
          gender,
          'true',
        );
        // Create application with INITIATED status
        const application = await this.applicationsRepository.create(
          uuid(),
          user.id,
          this.APPLICATION_STATUS.INITIATED.code,
          this.APPLICATION_STATUS.INITIATED.value,
        );
        // Update application to BASIC INFORMATION SUBMITTED status
        application.status = this.APPLICATION_STATUS.BASIC_INFORMATION_SUBMITTED.code;
        application.remarks = this.APPLICATION_STATUS.BASIC_INFORMATION_SUBMITTED.value;
        await this.applicationsRepository.update(application);
        
        // Set redis applicationId
        await this.redisClient.set(`core_${phoneNumber}`, application.id, 'EX', APPLICATION_EXPIRY);
        // await this._setRedis(`core_${phoneNumber}`, application.id, APPLICATION_EXPIRY);

        const response = Object.assign({
          statusCode: Status.CREATED,
          message: 'User and application created.',
          data: user, application
        });
        this.emit(SUCCESS, response);
      } else {
        const checkApplication = await this.applicationsRepository.getByUserId(checkUser.id);
        if (!checkApplication) {
          // Create application with INITIATED status
          const application = await this.applicationsRepository.create(
            uuid(),
            user.id,
            this.APPLICATION_STATUS.INITIATED.code,
            this.APPLICATION_STATUS.INITIATED.value,
          );
          // Update application to BASIC INFORMATION SUBMITTED status
          application.status = this.APPLICATION_STATUS.BASIC_INFORMATION_SUBMITTED.code;
          application.remarks = this.APPLICATION_STATUS.BASIC_INFORMATION_SUBMITTED.value;
          await this.applicationsRepository.update(application);

          // Set redis applicationId
          await this.redisClient.set(`core_${phoneNumber}`, application.id, 'EX', APPLICATION_EXPIRY);

          const response = Object.assign({
            statusCode: Status.CREATED,
            message: 'Application created.',
            data: {
              user: checkUser,
              application
            },
          });
          this.emit(SUCCESS, response);
        } else {
          // Set redis applicationId
          await this.redisClient.set(`core_${phoneNumber}`, checkApplication.id, 'EX', APPLICATION_EXPIRY);

          const response = Object.assign({
            statusCode: Status.ACCEPTED,
            message: 'User already exist, please go to sign in instead.',
            data: {
              user: checkUser,
              application: checkApplication,
            }
          });
          this.emit(SUCCESS_EXIST, response);
        }
      }
    } catch (error) {
      this.logger.error('Error on Public Controller - Sign Up', error);
      this.emit(ERROR, error);
    }
  }

  async signin(phoneNumber) {
    this.logger.info('Public Controller - Sign In', { phoneNumber });
    const { SUCCESS, REDIRECT, ERROR } = this.outputs;
    try {
      const user = await this.usersRepository.getByPhoneNumber(phoneNumber);
      if (user) {
        const application = await this.applicationsRepository.getByUserId(user.id);
        if (application) {
          if (
            application.status == this.APPLICATION_STATUS.BASIC_INFORMATION_SUBMITTED.code
            || application.status == this.APPLICATION_STATUS.OTP_SUBMISSION.code
          ) {
            // redirect to otp page
            const response = Object.assign({
              statusCode: Status.TEMPORARY_REDIRECT,
              message: 'redirect to otp page.',
              data: {
                redirectURL: this.WEB_URL.OTP,
              },
            });
            this.emit(REDIRECT, response);
          } else if (
            application.status == this.APPLICATION_STATUS.OTP_VERIFIED.code
          ) {
            // redirect to set pin page
            const response = Object.assign({
              statusCode: Status.TEMPORARY_REDIRECT,
              message: 'redirect to set pin page.',
              data: {
                redirectURL: this.WEB_URL.SET_PIN,
              },
            });
            this.emit(REDIRECT, response);
          } else {
            const response = Object.assign({
              statusCode: Status.OK,
              message: 'user found.',
            });
            this.emit(SUCCESS, response);
          }
        } else {
          const response = Object.assign({
            statusCode: Status.NOT_FOUND,
            message: `User not found with phone number: ${phoneNumber}`,
            error: 'application not found',
          });
          this.emit(NOT_FOUND, response);
        }
      } else {
        const response = Object.assign({
          statusCode: Status.NOT_FOUND,
          message: `User not found with phone number: ${phoneNumber}`,
          error: 'user not found',
        });
        this.emit(NOT_FOUND, response);
      }
    } catch (error) {
      this.logger.error('Error on Public Controller - Sign In', error);
      this.emit(ERROR, error);
    }
  }

  async generateOTP(phoneNumber, isRegister) {
    this.logger.info('Public Controller - Generate OTP', { phoneNumber });
    const { SUCCESS, OTP_SUSPEND, NOT_FOUND, ERROR } = this.outputs;
    try {
      const checkUser = await this.usersRepository.getByPhoneNumber(phoneNumber);
      if (checkUser) {
        // Mockup generate
        const random4DigitNum = '1234';
        // const random4DigitNum = await (Math.floor(Math.random() * 10000) + 10000).toString().substring(1);

        await this.redisClient.set(`core_otp_${phoneNumber}`, random4DigitNum, 'EX', OTP_EXPIRY);

        // Check how many times this phone number regenerate in threshold times
        const checkRegenerate = await this.redisClient.getAsync(`core_otp_regen_counter_${phoneNumber}`);
        if (!checkRegenerate) {
          await this.redisClient.set(`core_otp_regen_counter_${phoneNumber}`, 0, 'EX', OTP_EXPIRY);
          await this.redisClient.incr(`core_otp_regen_counter_${phoneNumber}`);
        } else {
          if (checkRegenerate < OTP_COUNTER) {
            await this.redisClient.incr(`core_otp_regen_counter_${phoneNumber}`);
          } else {
            await this.redisClient.set(`core_otp_regen_counter_${phoneNumber}`, 0, 'EX', BLOCK_EXPIRY);
          }
        }

        // Check if blocked
        const isBlocked =
          await this.redisClient.getAsync(`core_otp_regen_counter_${phoneNumber}`) == 'BLOCKED'
          ? true
          : false;

        // If isRegister, update application to OTP SUBMISSION status
        if (isRegister) {
          const applicationId = await this.redisClient.getAsync(`core_${phoneNumber}`);
          if (applicationId) {
            this.logger.info(`Updating applicationId: ${applicationId} to OTP_SUBMISSION`);
            await this.applicationsRepository.update({
              id: applicationId,
              status: this.APPLICATION_STATUS.OTP_SUBMISSION.code,
              remarks: this.APPLICATION_STATUS.OTP_SUBMISSION.value,
            });
          } else {
            const user = await this.usersRepository.getByPhoneNumber(phoneNumber);
            const application = await this.applicationsRepository.getByUserId(user.id);
            application.status = this.APPLICATION_STATUS.OTP_SUBMISSION.code,
            application.remarks = this.APPLICATION_STATUS.OTP_SUBMISSION.value,
            await this.applicationsRepository.update(application);
          }
        }
        
        if (isBlocked) {
          const resposne = Object.assign({
            statusCode: Status.LOCKED, //423
            message: `OTP Temporary suspended.`,
          });
          this.emit(OTP_SUSPEND, resposne);
        } else {
          const response = Object.assign({
            statusCode: Status.OK,
            message: 'OTP Generate!',
          });
          this.emit(SUCCESS, response);
        }
      } else {
        const response = Object.assign({
          statusCode: Status.NOT_FOUND,
          message: `User with phone number ${phoneNumber} not found.`,
        });
        this.emit(NOT_FOUND, response);
      }
    } catch (error) {
      this.logger.error('Error on Public Controller - Generate OTP', error);
      this.emit(ERROR, error);
    }
  }

  async verifyOTP(phoneNumber, otp, isRegister) {
    this.logger.info('Public Controller - Verify OTP', { phoneNumber });
    const { SUCCESS, OTP_EXPIRED, OTP_NOT_VALID, ERROR } = this.outputs;
    try {
      const redisOTP = await this.redisClient.getAsync(`core_otp_${phoneNumber}`);
      this.logger.debug(`VERIFY OTP, PHONE NUMBER: ${phoneNumber}`, { otp, redisOTP });
      if (!redisOTP) {
        const response = Object.assign({
          statusCode: Status.LOCKED, // 423
          message: 'OTP expired!',
        });
        this.emit(OTP_EXPIRED, response)
      } else {
        if (otp != redisOTP) {
          const response = Object.assign({
            statusCode: Status.LOCKED, // 423
            message: 'OTP not valid!',
          });
          this.emit(OTP_NOT_VALID, response);
        } else {
          // If isRegister, update application to OTP SUBMISSION status
          if (isRegister) {
            const applicationId = await this.redisClient.getAsync(`core_${phoneNumber}`);
            if (applicationId) {
              this.logger.info(`Updating applicationId: ${applicationId} to OTP_VERIFIED`);
              await this.applicationsRepository.update({
                id: applicationId,
                status: this.APPLICATION_STATUS.OTP_VERIFIED.code,
                remars: this.APPLICATION_STATUS.OTP_VERIFIED.value,
              });
            } else {
              this.logger.info(`Updating applicationId: ${applicationId} to OTP_VERIFIED`);
              const user = await this.usersRepository.getByPhoneNumber(phoneNumber);
              const application = await this.applicationsRepository.getByUserId(user.id);
              application.status = this.APPLICATION_STATUS.OTP_VERIFIED.code,
              application.remarks = this.APPLICATION_STATUS.OTP_VERIFIED.value,
              await this.applicationsRepository.update(application);
            }
          }
          
          const response = Object.assign({
            statusCode: Status.OK,
            message: 'OTP valid.',
          });
          this.emit(SUCCESS, response);
        }
      }
    } catch (error) {
      this.logger.error('Error on Public Controller - Verify OTP', error);
      this.emit(ERROR, error);
    }
  }

  async setPIN(phoneNumber, pin) {
    this.logger.info('Public Controller - Set PIN', { phoneNumber });
    const { SUCCESS, NOT_FOUND, ERROR} = this.outputs;
    try {
      const checkUser = await this.usersRepository.getByPhoneNumber(phoneNumber);
      if (checkUser) {
        // Hash pin and update to database [will update this code]
        const salt = await bcrypt.genSaltSync(10);
        const hashPin = await bcrypt.hashSync(pin, salt);
        await this.usersRepository.changePin(checkUser.id, hashPin);

        // If applicationId exist, update to SET PIN status
        const applicationId = await this.redisClient.getAsync(`core_${phoneNumber}`);
        if (applicationId) {
          this.logger.info(`Updating applicationId: ${applicationId} to SET_PIN`);
          await this.applicationsRepository.update({
            id: applicationId,
            status: this.APPLICATION_STATUS.SET_PIN.code,
            remarks: this.APPLICATION_STATUS.SET_PIN.value,
          });
          // Update to BASIC APPLICATION SUBMITTED
          await this.applicationsRepository.update({
            id: applicationId,
            status: this.APPLICATION_STATUS.BASIC_APPLICATION_SUBMITTED.code,
            remarks: this.APPLICATION_STATUS.BASIC_APPLICATION_SUBMITTED.value,
          });
        } else {
          this.logger.info(`Updating applicationId: ${applicationId} to SET_PIN`);
          const user = await this.usersRepository.getByPhoneNumber(phoneNumber);
          const application = await this.applicationsRepository.getByUserId(user.id);
          application.status = this.APPLICATION_STATUS.SET_PIN.code,
          application.remarks = this.APPLICATION_STATUS.SET_PIN.value,
          await this.applicationsRepository.update(application);
          // Update to BASIC APPLICATION SUBMITTED
          application.status = this.APPLICATION_STATUS.BASIC_APPLICATION_SUBMITTED.code,
          application.remarks = this.APPLICATION_STATUS.BASIC_APPLICATION_SUBMITTED.value,
          await this.applicationsRepository.update(application);
        }

        const response = Object.assign({
          statusCode: Status.OK,
          message: 'PIN Set!',
        });
        this.emit(SUCCESS, response);
      } else {
        const response = Object.assign({
          statusCode: Status.NOT_FOUND,
          message: `User with phone number ${phoneNumber} not found.`,
        });
        this.emit(NOT_FOUND, response);
      }
    } catch (error) {
      this.logger.error('Error on Public Controller - Set PIN', error);
      this.emit(ERROR, error);
    }
  }

  async verifyPIN(phoneNumber, pin) {
    this.logger.info('Public Controller - Verify PIN', { phoneNumber });
    const { SUCCESS, PIN_NOT_VALID, NOT_FOUND, ERROR } = this.outputs;
    try {
      const user = await this.usersRepository.getByPhoneNumber(phoneNumber);
      if (user) {
        const compare = await bcrypt.compareSync(pin, user.pin);
        if (compare) {
          const response = Object.assign({
            statusCode: Status.OK,
            message: 'phone number and pin valid.',
          });
          this.emit(SUCCESS, response);
        } else {
          const response = Object.assign({
            statusCode: Status.FORBIDDEN,
            message: 'invalid pin.',
          });
          this.emit(PIN_NOT_VALID, response);
        }
      } else {
        const response = Object.assign({
          statusCode: Status.NOT_FOUND,
          message: `User not found with phone number: ${phoneNumber}`,
          error: 'user not found',
        })
        this.emit(NOT_FOUND, response);
      }
    } catch (error) {
      this.logger.error('Error on Public Controller - Verify OTP', error);
      this.emit(ERROR, error);
    }
  }

}

PublicControllers.setOutputs([
  'SUCCESS',
  'SUCCESS_EXIST',
  'REDIRECT',
  'OTP_SUSPEND',
  'OTP_EXPIRED',
  'OTP_NOT_VALID',
  'PIN_NOT_VALID',
  'NOT_FOUND',
  'ERROR'
]);

module.exports = PublicControllers;
