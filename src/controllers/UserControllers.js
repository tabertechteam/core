const Operation = require('./Operation');

class UserControllers extends Operation{
  constructor({ logger, constants, usersRepository, applicationsRepository, redisClient }) {
    super();
    this.logger = logger;
    this.usersRepository = usersRepository;
    this.applicationsRepository = applicationsRepository;
    this.redisClient = redisClient;

    // Declare specific constants
    this.APPLICATION_STATUS = constants.APPLICATION_STATUS;
    this.WEB_URL = constants.WEB_URL;
  }

  async signup(fullName, phoneNumber, email, gender) {
    this.logger.info('User Controller - Sign Up', { fullName, phoneNumber, email, gender });
    const { SUCCESS, SUCCESS_EXIST, ERROR } = this.outputs;
    try {
      const checkUser = await this.usersRepository.getByPhoneNumber(phoneNumber);
      if (!checkUser) {
        // Create user
        const user = await this.usersRepository.create(
          uuid(),
          fullName,
          phoneNumber,
          email,
          gender,
          'true',
        );

        // Create application with INITIATED status
        const application = await this.applicationsRepository.create(
          uuid(),
          user.id,
          this.APPLICATION_STATUS.INITIATED.code,
          this.APPLICATION_STATUS.INITIATED.value,
        );

        // Update application to BASIC INFORMATION SUBMITTED status
        application.status = this.APPLICATION_STATUS.BASIC_INFORMATION_SUBMITTED.code;
        application.remarks = this.APPLICATION_STATUS.BASIC_INFORMATION_SUBMITTED.value;
        await this.applicationsRepository.update(application);
        
        // Set redis applicationId
        await this.redisClient.set(`core_${phoneNumber}`, application.id, 'EX', APPLICATION_EXPIRY);
        // await this._setRedis(`core_${phoneNumber}`, application.id, APPLICATION_EXPIRY);

        const response = Object.assign({
          statusCode: Status.CREATED,
          message: 'User and application created.',
          data: user, application
        });
        this.emit(SUCCESS, response);
      } else {
        // Check application if exist
        const checkApplication = await this.applicationsRepository.getByUserId(checkUser.id);
        if (!checkApplication) {
          // Create application with INITIATED status
          const application = await this.applicationsRepository.create(
            uuid(),
            user.id,
            this.APPLICATION_STATUS.INITIATED.code,
            this.APPLICATION_STATUS.INITIATED.value,
          );
          
          // Update application to BASIC INFORMATION SUBMITTED status
          application.status = this.APPLICATION_STATUS.BASIC_INFORMATION_SUBMITTED.code;
          application.remarks = this.APPLICATION_STATUS.BASIC_INFORMATION_SUBMITTED.value;
          await this.applicationsRepository.update(application);

          // Set redis applicationId
          await this.redisClient.set(`core_${phoneNumber}`, application.id, 'EX', APPLICATION_EXPIRY);

          const response = Object.assign({
            statusCode: Status.CREATED,
            message: 'Application created.',
            data: {
              user: checkUser,
              application
            },
          });
          this.emit(SUCCESS, response);
        } else {
          // Set redis applicationId
          await this.redisClient.set(`core_${phoneNumber}`, checkApplication.id, 'EX', APPLICATION_EXPIRY);

          const response = Object.assign({
            statusCode: Status.ACCEPTED,
            message: 'User already exist, please go to sign in instead.',
            data: {
              user: checkUser,
              application: checkApplication,
            }
          });
          this.emit(SUCCESS_EXIST, response);
        }
      }
    } catch (error) {
      this.logger.error('Error on Public Controller - Sign Up', error);
      this.emit(ERROR, error);
    }
  }

  async signin(phoneNumber) {
    this.logger.info('User Controller - Sign In', { phoneNumber });
    const { SUCCESS, REDIRECT, ERROR } = this.outputs;
    try {
      // Check user if exist
      const user = await this.usersRepository.getByPhoneNumber(phoneNumber);
      if (user) {
        const application = await this.applicationsRepository.getByUserId(user.id);
        if (application) {
          if (
            application.status == this.APPLICATION_STATUS.BASIC_INFORMATION_SUBMITTED.code
            || application.status == this.APPLICATION_STATUS.OTP_SUBMISSION.code
          ) {
            // redirect to otp page
            const response = Object.assign({
              statusCode: Status.TEMPORARY_REDIRECT,
              message: 'redirect to otp page.',
              data: {
                redirectURL: this.WEB_URL.OTP,
              },
            });
            this.emit(REDIRECT, response);
          } else if (
            application.status == this.APPLICATION_STATUS.OTP_VERIFIED.code
          ) {
            // redirect to set pin page
            const response = Object.assign({
              statusCode: Status.TEMPORARY_REDIRECT,
              message: 'redirect to set pin page.',
              data: {
                redirectURL: this.WEB_URL.SET_PIN,
              },
            });
            this.emit(REDIRECT, response);
          } else {
            const response = Object.assign({
              statusCode: Status.OK,
              message: 'user found.',
            });
            this.emit(SUCCESS, response);
          }
        } else {
          const response = Object.assign({
            statusCode: Status.NOT_FOUND,
            message: `User not found with phone number: ${phoneNumber}`,
            error: 'application not found',
          });
          this.emit(NOT_FOUND, response);
        }
      } else {
        const response = Object.assign({
          statusCode: Status.NOT_FOUND,
          message: `User not found with phone number: ${phoneNumber}`,
          error: 'user not found',
        });
        this.emit(NOT_FOUND, response);
      }
    } catch (error) {
      this.logger.error('Error on User Controller - Sign In', error);
      this.emit(ERROR, error);
    }
  }
}

UserControllers.setOutputs([]);

module.exports = UserControllers;
