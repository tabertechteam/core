const uuid = require('uuid/v4');

module.exports = ({
  logger,
  constants,
  Bull,
  usersRepository,
  taberRepository,
  wavecellService,
}) => {
  // Declare specific constants
  const { QUEUE_INSTANCES } = constants;
  const InsertBulkUsers = Bull.queueInstance(QUEUE_INSTANCES.INSERT_BULK_USERS);

  InsertBulkUsers.process(async function (job) {
    logger.info(`InsertBulkUsers: ${QUEUE_INSTANCES.INSERT_BULK_USERS}`, job);
    const { users, jointSavingAccountId } = job.data;
    return await insertBulkUsers(users, jointSavingAccountId);
  });

  async function insertBulkUsers(users, jointSavingAccountId) {
    const newUser = Object.assign({
      destinations: new Array(),
      template: {
        source: 'TABER',
        text: 'Ini akun yang belum daftar',
      },
    });
    const oldUser = Object.assign({
      destinations: new Array(),
      template: {
        source: 'TABER',
        text: 'Ini akun yang sudah daftar',
      },
    });
    for (const user of users) {
      const { fullName, phoneNumber } = user;
      const checkUser = await usersRepository.getByPhoneNumber(phoneNumber);
      if (checkUser) {
        await taberRepository.addMember(checkUser.id, jointSavingAccountId);
        await oldUser.destinations.push(phoneNumber);
      } else {
        const createUser = await usersRepository.create(uuid(), fullName, phoneNumber, null, null, 'false');
        await taberRepository.addMember(createUser.id, jointSavingAccountId);
        await newUser.destinations.push(phoneNumber);
      }
    }

    // Send sms notification
    if (newUser.destinations.length > 1) await wavecellService.sendManyCompact(newUser.destinations, newUser.template);
    if (oldUser.destinations.length > 1) await wavecellService.sendManyCompact(oldUser.destinations, oldUser.template);

    return 'SUCCESS';
  };

  return InsertBulkUsers;
};
