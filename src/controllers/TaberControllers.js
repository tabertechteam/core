const Operation = require('./Operation');
const uuid = require('uuid/v4');
const Status = require('http-status');

class TaberControllers extends Operation {
  constructor({ logger, constants, taberRepository, Bull }) {
    super();
    this.logger = logger;
    this.taberRepository = taberRepository;
    this.Bull = Bull;
    
    // Declare specific constants
    this.JSA_TYPE = constants.JOINT_SAVING_ACCOUNT_TYPE;
    this.JSA_STATUS = constants.JOINT_SAVING_ACCOUNT_STATUS;
    this.CURRENCY = constants.CURRENCY;
    this.LIST_CURRENCY = constants.LIST_CURRENCY;
    this.QUEUE_INSTANCES = constants.QUEUE_INSTANCES;
  }

  async createTaber(
    userId,
    name,
    currency,
    description,
    targetSavers,
    targetAmountSavers,
    savingTargetTime,
    savingDepositTime,
    savingDepositType,
    savingGroup,
    memberInvitationList,
  ) {
    this.logger.info('Taber Controller - Create Taber', {
      userId,
      name,
      currency,
      description,
      targetSavers,
      targetAmountSavers,
      savingTargetTime,
      savingDepositTime,
      savingDepositType,
      savingGroup,
      memberInvitationList,
    });
    const { SUCCESS, ERROR } = this.outputs;
    try {
      // Create jointSavingAccount object
      const jointSavingAccountObj = Object.assign({
        id: uuid(),
        creatorId: userId,
        type: this.JSA_TYPE.BASIC,
        status: this.JSA_STATUS.ACTIVE,
        balance: 0,
        name,
        currency,
        description,
        targetSavers,
        targetAmountSavers,
        savingTargetTime,
        savingDepositTime,
        savingDepositType,
      });
      const jointSavingAccount = await this.taberRepository.createJSA(jointSavingAccountObj, savingGroup);

      // Create producers
      const InsertBulkUsers = await this.Bull.queueInstance(this.QUEUE_INSTANCES.INSERT_BULK_USERS);
      InsertBulkUsers.add({
          users: memberInvitationList,
          jointSavingAccountId: jointSavingAccount.id
        }, { attempts: 5, backoff: { type: 'fixed', delay: 5000 } });

      const response = Object.assign({
        statusCode: Status.CREATED,
        message: 'Taber created.',
        data: {
          jointSavingAccount,
        },
      });
      this.emit(SUCCESS, response);
    } catch (error) {
      this.logger.error('Error on Taber Controller - Create Taber', error);
      this.emit(ERROR, error);
    }

  }
}

TaberControllers.setOutputs(['SUCCESS', 'ERROR']);

module.exports = TaberControllers;
