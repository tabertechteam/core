const Queue = require('bull');

module.exports.queueInstance = (name) => {
  return new Queue(name, {
    redis: {
      host: process.env.REDIS_HOST || '127.0.0.1',
      port: process.env.REDIS_PORT || '6379',
    },
    limiter: {
      max: 5,
      duration: 5000,
    },
  });
};
