// Application
const APPLICATION_STATUS = {
  INITIATED: {
    code: 1,
    value: 'initiated',
  },
  BASIC_INFORMATION_SUBMITTED: {
    code: 2,
    value: 'basic information submitted',
  },
  OTP_SUBMISSION: {
    code: 3,
    value: 'otp submission',
  },
  OTP_VERIFIED: {
    code: 4,
    value: 'otp verified',
  },
  SET_PIN: {
    code: 5,
    value: 'set pin',
  },
  BASIC_APPLICATION_SUBMITTED: {
    code: 6,
    value: 'basic application submitted',
  },
  UNDEFINED: {
    code: -1,
    value: 'undefined'
  },
};

// Joint Saving
const JOINT_SAVING_ACCOUNT_TYPE = {
  BASIC: 'BASIC',
  BANK_CONNECTED: 'BANK_CONNECTED',
};

const JOINT_SAVING_ACCOUNT_STATUS = {
  ACTIVE: 'ACTIVE',
  INACTIVE: 'INACTIVE',
};

const CURRENCY = {
  IDR: 'IDR', // Indonesia => Rupiah
  MYR: 'MYR', // Malayasia => Ringgit
  SGD: 'SGD', // Singapore => Dolar Singapur
  THB: 'THB', // Thailand => Baht
  VND: 'VND', // Vietnam => Dong
  PHP: 'PHP', // Filipina => Peso
  MMK: 'MMK', // Myanmar => Kyat
  KHR: 'KHR', // Kamboja => Riel Kamboja
  LAK: 'LAK', // Laos => Lao Kop
  BND: 'BND', // Brunei => Dolar Brunei
  USD: 'USD', // US / America / Timor Lester => Dolar US
  // AED: 'AED', // Uni Emirat Arab => Dirham
  // MAD: 'MAD', // Maroko / Sahara Barat => Dirham
  // BHD: 'BHD', // Bahrain => Dinar Bahrain
  // DZD: 'DZD', // Aljazair => Dinar Aljazair
  // IQD: 'IQD', // Irak => Dinar Irak
  // JOD: 'JOD', // Yordania => Dinar Yordania
  // KWD: 'KWD', // Kuwait => Dinar Kuwait
  // LYD: 'LYD', // Libya => Dinar Libya
  // RSD: 'RSD', // Serbia => Dinar Serbia
  // TND: 'TND', // Tunisia => Dinar Tunisia
  // SDD: 'SDD', // Sudan => Dinar Sudan
  // YDD: 'YDD', // Yaman Selatan => Dinar Yaman Selatan
  // YUD: 'YUD', // Yugoslavia baru => Dinar Yugoslavia baru
  // YUM: 'YUM', // Yugoslavia => Dinar Yugoslavia
  ['Dinar-Emas']: 'Dinar-Emas',
  ['Dinar-Perak']: 'Dinar-Perak',
};

const LIST_CURRENCY = [
  'IDR',
  'MYR',
  'SGD',
  'THB',
  'VND',
  'PHP',
  'MMK',
  'KHR',
  'LAK',
  'BND',
  'USD',
  // 'AED',
  // 'MAD',
  // 'BHD',
  // 'DZD',
  // 'IQD',
  // 'JOD',
  // 'KWD',
  // 'LYD',
  // 'RSD',
  // 'TND',
  // 'SDD',
  // 'YDD',
  // 'YUD',
  // 'YUM',
  'Dinar-Emas',
  'Dinar-Perak',
];

const JOINT_SAVING_ACCOUNT_TRANSACTION_TYPE = {

};

const JOINT_SAVING_ACCOUNT_TRANSACTION_STATUS = {

};

const JOINT_SAVING_MEMBER_TYPE = {
  ADM_COLLECTOR: 'ADM_COLLECTOR',
  COLLECTOR: 'COLLECTOR',
  SAVER: 'SAVER',
};

const JOINT_SAVING_RATING_TYPE = {
  RATE_SAVER: 'RATE_SAVER',
  RATE_COLLECTOR: 'RATE_COLLECTOR',
  RATE_JOINT_SAVING: 'RATE_JOINT_SAVING',
};

// Others
const WEB_URL = {
  SIGN_UP: process.env.WEB_SIGN_UP_URL || '',
  OTP: process.env.WEB_OTP_URL || '',
  SET_PIN: process.env.WEB_SET_PIN_URL || '',
};

const QUEUE_INSTANCES = {
  INSERT_BULK_USERS: 'insert-bulk-users',
};

module.exports = {
  APPLICATION_STATUS,
  JOINT_SAVING_ACCOUNT_TYPE,
  JOINT_SAVING_ACCOUNT_STATUS,
  CURRENCY,
  LIST_CURRENCY,
  JOINT_SAVING_ACCOUNT_TRANSACTION_TYPE,
  JOINT_SAVING_ACCOUNT_TRANSACTION_STATUS,
  JOINT_SAVING_MEMBER_TYPE,
  JOINT_SAVING_RATING_TYPE,
  WEB_URL,
  QUEUE_INSTANCES,
};
