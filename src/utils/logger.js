const moment = require('moment');

// Buat label bentuknya array
// Buat ngesave bentuk file .log
// Buat layer (controller, service, repository)
class Logger {
  constructor() {
    this.level = {
      debug: 'debug',
      info: 'info',
      error: 'error',
    };
  }

  // Log
  log(level, msg, details = null) {
    const log = this._generateLog(
      level,
      msg,
      details,
    );
    console.log(log);
    return log;
  }


  // Debug
  debug(msg) {
    const log = this._generateLog({
      leve: this.level.debug,
      msg,
    });
    console.log(log);
    return log;
  }
  debug(msg, details = null) {
    const log = this._generateLog({
      leve: this.level.debug,
      msg,
      details,
    });
    console.log(log);
    return log;
  }
  // Info
  info(msg) {
    const log = this._generateLog({
      level: this.level.info,
      msg,
    });
    console.log(log);
    return log;
  }
  info(msg, details = null) {
    const log = this._generateLog({
      level: this.level.info,
      msg,
      details,
    });
    console.log(log);
    return log;
  }

  // Error
  error(msg) {
    const log = this._generateLog({
      level: this.level.error,
      msg,
    });
    console.log(log);
    return log;
  }
  error(msg, details = null) {
    const log = this._generateLog({
      level: this.level.error,
      msg,
      details,
    });
    console.log(log);
    return log;
  }

  // Private
  _generateLog({level, msg, details = null}) {
    const log = new Object();
    log.timestamp = this._generateTime();;
    log.level = level;
    log.msg = msg;
    log.details = details ? details : false;
    return JSON.stringify(log);
  };
  _generateTime() {
    return moment().format('DD/MM/YYYY hh:mm:ss')
  }

}

module.exports = Logger;
