class AuthenticationService {
  constructor({ logger, mongooseUsersRepository, mongooseTokenRepository }) {
    this.logger = logger;
    this.usersRepository = mongooseUsersRepository;
    this.tokenRepository = mongooseTokenRepository;
  }


}

module.exports = AuthenticationService;
