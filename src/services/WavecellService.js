require('dotenv').config();
const axios = require('axios');

class WavecellService {
  constructor({ logger }) {
    this.logger = logger;
    this.wavecell = axios.create({
      baseURL: process.env.WAVECELL_BASE_URL + '/' + process.env.WAVECELL_SUB_ACCOUNT_ID,
      timeout: process.env.WAVECELL_TIMEOUT || '10000',
      headers: {
        ['Authorization']: `Bearer ${process.env.WAVECELL_API_KEY}`,
        ['Content-Type']: 'application/json',
      },
    });
    this.wavecell.interceptors.request.use((req) => {
      this.logger.info('Request send to wavecell.', req);
      return req;
    });
    this.wavecell.interceptors.response.use((res) => {
      this.logger.info('Response from wavecell.', res.data);
      return res;
    }, (error) => {
      if (error.response.data) {
        this.logger.error('Error response from wavecell', error.response.data);
      } else {
        this.logger.error('Error response from wavecell', error);
      }
      return Promise.reject(error);
    })
  }

  async sendOne(source, destination, text) {
    try {
      const response = await this.wavecell({
        method: 'POST',
        url: `/single`,
        data: {
          source,
          destination,
          text,
          // encoding: 'AUTO',
        },
      });
      return response.data;
    } catch (error) {
      throw error;
    }
  }

  async sendManyCompact(destinations, template) {
    try {
      const response = await this.wavecell({
        method: 'POST',
        url: '/many/compact',
        data: {
          destinations,
          template,
        },
      });
      return response.data;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = WavecellService;
// const Logger = require('../utils/logger');
// const wavecell = new WavecellService({ logger: new Logger });
// wavecell.sendOne('+6282121765388', 'Test dari taber');



// https://api.wavecell.com/sms/v1/TaberAdmin_9yYA0_hq/single
// https://api.wavecell.com/sms/v1/TaberAdmin_9yYA0_hq/single