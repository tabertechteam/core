const { createContainer, asClass, asFunction, asValue } = require('awilix');
const { scopePerRequest } = require('awilix-express');

// Server
const Server = require('./Server');
const router = require('./routes/router');
const logger = require('./utils/logger');
const config = require('../config');

// Middlewares
const AuthorizationMiddlewares = require('./middlewares/AuthorizationMiddlewares');

// Controllers
const {
  PublicControllers,
  TaberControllers,
} = require('./controllers');

// Queue
// Producers
// require('./controllers/Queue/Consumers/InsertBulkUsers');
const InserBulkUsers = require('./controllers/Queue/Consumers/InsertBulkUsers');

// Consumers

// Listeners

// Repository
// Sequelize
const {
  SequelizeApplicationsRepository,
  SequelizeUsersRepository,
  SequelizeTaberRepository,
} = require('./repository');
// Mongoose
const {
  MongooseUsersRepository,
  MongooseTokenRepository,
} = require('./repository');

// Models
const {
  sequelize: database,
  users: UserModel,
  applications: ApplicationModel,
  joint_saving_member: JointSavingMemberModel,
  joint_saving_acount: JointSavingAccountModel,
  joint_saving_acount_group: JointSavingAccountGroupModel,
  joint_saving_acount_transaction: JointSavingAccountTransactionModel,
  joint_saving_rating: JointSavingRatingModel,
} = require('./database/models');

// MongoDB
const {
  mongoose,
  token: TokenSchema,
  users: UserSchema,
} = require('./database/mongodb');

// Services
const {
  WavecellService,
} = require('./services');

// Utils
const constants = require('./utils/constants');
const { redisClient } = require('./utils/redis');
const Bull = require('./utils/queue');

// ==========================================

// Initialize container
const container = createContainer();

// Server
container
  .register({
    server: asClass(Server).singleton(),
  })
  .register({
    router: asFunction(router).singleton(),
    logger: asClass(logger).singleton(),
  })
  .register({
    config: asValue(config),
  });


// Middlewares
container.register({
  containerMiddleware: asValue(scopePerRequest(container)),
  authorizationMiddlewares: asValue(AuthorizationMiddlewares),
});

// Controllers
container.register({
    publicControllers: asClass(PublicControllers),
    taberControllers: asClass(TaberControllers),
});

// Queue
container
  .register({
    inserBulkUsers: asValue(InserBulkUsers),
  });

// Repository
// Sequelize
container.register({
    applicationsRepository: asClass(SequelizeApplicationsRepository).singleton(),
    usersRepository: asClass(SequelizeUsersRepository).singleton(),
    taberRepository: asClass(SequelizeTaberRepository).singleton(),
});
// Mongoose
container.register({
  mongooseUsersRepository: asClass(MongooseUsersRepository).singleton(),
  mongooseTokenRepository: asClass(MongooseTokenRepository).singleton(),
});

// Models
container.register({
    database: asValue(database),
    userModel: asValue(UserModel),
    applicationModel: asValue(ApplicationModel),
    jointSavingMemberModel: asValue(JointSavingMemberModel),
    jointSavingAccountModel: asValue(JointSavingAccountModel),
    jointSavingAccountGroupModel: asValue(JointSavingAccountGroupModel),
    jointSavingAccountTransactionModel: asValue(JointSavingAccountTransactionModel),
    jointSavingRatingModel: asValue(JointSavingRatingModel),
});

// MongoDB
container.register({
  mongoose: asValue(mongoose),
  userSchema: asValue(UserSchema),
  tokenSchema: asValue(TokenSchema),
});

// Services
container.register({
  wavecellService: asClass(WavecellService),
});

// Utils
container.register({
    constants: asValue(constants),
    redisClient: asValue(redisClient),
    Bull: asValue(Bull),
});

module.exports = container;
